import { LightningElement, track } from 'lwc';

export default class MaskInputDemo extends LightningElement {

    @track maskedInput;
    @track idNumberMaxLength;

    handleInputChange(event){
        let fieldName = event.target.name;
        if(fieldName == 'accountNumber'){
            maskInput(event.target.name, event.target.value);
        }
    }

    maskInput(input, currentVal) {
        this.idNumberMaxLength = 10;
        var newVal = currentVal;
    
        if (newVal.length == this.idNumberMaxLength) {
          var ssnLength = newVal.length;
          var ssn = newVal.substring(ssnLength - 4, ssnLength);
          newVal = "**-***" + ssn ; // Mask according to the requirement
          this.maskedInput = newVal;
        }
      }
}